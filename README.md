# Newsprint Simulator

See approximately what your font would look like when printed at small sizes on newsprint.

Supports both Glyphs and Robofont (see `... - Glyphs.py` and `... - Robofont.py` versions of the script).
