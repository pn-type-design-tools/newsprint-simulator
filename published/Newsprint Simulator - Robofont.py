#MenuTitle: Newsprint Simulator
# -*- coding: utf-8 -*-
__doc__=''' See what your font will look like on newsprint.'''


# ———————————————————————————————————
# ———— SIMULATE NEWSPRINT (v1.0) ————
# ——————————— for Robofont ——————————
# ———————— by @pnowelldesign ————————
# ———————————————————————————————————


# ———————————————————————————————
# USER-ADJUSTABLE SETTINGS:

debug = False
useCurrentFont = True

pdfFileNamePrefix = 'Newsprint Test' # without .pdf

blurDefault = 8
blurMin = 0
blurMax = 16
contrastMin = 1
contrastMax = 3

# Fallback font (Postscript Name or path to OTF font file)
fontPSName = 'Didot'
    # CovikSansMono-Regular
    # FernMicro-Regular
    # Didot

ptSizes = [72, 36, 24, 18, 16, 14, 12, 10, 8]
contentPresets = [
    u'hamburgefonts' ,
    u'the quick brown fox jumped over the lazy dog'
]

# ————————————————————————

# Which software is running the script?
# manually set for now... auto-detect in the future
RF = True
GL = False
DB = False

import os
import shutil # to remove/replace a folder (for font files)
import time
from datetime import datetime
from drawBot import *
from drawBot.ui.drawView import DrawView
from vanilla import *
from AppKit import NSImage, NSImageNameFolder
try :
    if GL :
        from GlyphsApp import *
    elif RF :
        from mojo.UI import GetFolder, Message, AskString
        from mojo import roboFont
        rofo = roboFont
        RFVersion = float( rofo.version )

    global thisFont
    if GL :
        thisFont = Glyphs.font
    elif RF :
        thisFont = CurrentFont()
except : # if these imports fail, the script is running in Drawbot
    DB = True
    pass


previewW = 72 * 9
previewH = 72 * 5
previewAspectRatio = float(previewH) / previewW
letterWidth = 72 * 11
letterHeight = 72 * 8.5
utilityFontFam = 'Avenir-Book'
utilityFontSize = 10
fontSizeForImage = 240
imgW = 7000
imgH = fontSizeForImage * 1.6 # give image bounds space around text
fontFolderName = '_Proof Fonts'
destinationFolderPath = os.path.expanduser('~/Desktop') # fallback to desktop


def valAsPercentage( val, vMin, vMax ) :
    percentage = float( (val - vMin) ) / (vMax - vMin) * 100
    return percentage

def drawDebugBox(x=0, y=0, w=0, h=0, color=(.2, .2, 1, 1), thickness=2) :
    with savedState():
        stroke( *color ) # default is blue
        strokeWidth( thickness )
        fill(0,0,0,0) # transparent fill
        rect( x,y,w,h )

def drawRenderedText( textPath, xPos=0, yPos=0, ptSize=72 ) :
    targetHeight = ptSize # pt
    scaleRatio = float(targetHeight) / imgH
    # Create a utility label for the font size
    with savedState():
        labelContent = str(ptSize)+'pt'
        font( utilityFontFam, utilityFontSize )
        utilityLabelSize = textSize(labelContent, align='right')
        labelPosY = yPos + ptSize/3 - utilityFontSize*.9
        box = (xPos-48, labelPosY) + utilityLabelSize
        textBox( labelContent, box, align='right'  )
    # Create a copy of the textPath element (prevent stacking of transforms)
    newPath = textPath.copy()
    # Scale and position it
    newPath.scale(x=scaleRatio, center=(0, 0))
    newPath.translate(xPos, yPos)
    # Draw it
    drawPath( newPath )
    # Draw a debug box around the traced image text bounds
    if debug :
        drawDebugBox( xPos, yPos, imgW*scaleRatio, ptSize, thickness=.2 )

def positionPreviewInLetterPage() :
    # figure out how much to move it in order to center it in a letter landscape page
    # how far away is the edge of a letter page (diff in width)?
    widthDifference = letterWidth - previewW
    heightDifference = letterHeight - previewH
    # translate by that amount
    translate( widthDifference/2, heightDifference/2 )
    # return the page size to letter landscape
    #return letterWidth, letterHeight

def getPDFFilePath() :
    # assembles the file path for an exported PDF
    exportFilePath = destinationFolderPath + '/' + pdfFileNamePrefix + ' ' + fontPSName + '.pdf'
    return exportFilePath

def exportPDF() :
    exportFilePath = getPDFFilePath()
    # save as PDF
    saveImage( exportFilePath )

def appendTimestampToFilePath( sourcePath ):
    try:
        stamp = datetime.now().strftime( "%Y-%m-%d-%H-%M-%S" )
        nakedPath, extension = os.path.splitext( sourcePath )
        newPath = "%s-%s%s" % ( nakedPath, stamp, extension )
        os.rename( sourcePath, newPath ) # renames the actual file
        return newPath
    except Exception as e:
        print( 'ERROR: could not timestamp the string.' )
        print( e )
        return sourcePath

def SETUP_setProofFolder( windowThatCalledIt ) :
    # return folder path   ——— Modified from proofing script
    global destinationFolderPath
    if not DB :
        # ask for folder path to save to
        userSpecifiedFolder = GetFolder()
        # if the user chose a folder (didn't hit cancel button)
        if userSpecifiedFolder :
            destinationFolderPath = userSpecifiedFolder # use that folder
    return destinationFolderPath

def SETUP_exportCurrentFont( destinationFolderPath ) :
    if useCurrentFont == False and not DB:
        return

    # Create/Replace the Font Files Folder
    global exportedFontFolderPath
    exportedFontFolderPath = destinationFolderPath + '/' + fontFolderName + '/'
    # If the folder already exists
    if os.path.exists( exportedFontFolderPath ):
        shutil.rmtree( exportedFontFolderPath ) # delete it
    # Create new folder
    os.makedirs( exportedFontFolderPath )

    # Start a list of all exported font files
    global exportedFontFiles
    exportedFontFiles = []

    if GL :
        allInstances = [ thisFont.instances[0] ] # Just support one instance for now
            # In the future, change to:   allInstances = thisFont.instances
        # Go through each instance...
        for instance in allInstances :
            # Export this instance:
            try:
                exportResult = instance.generate(OTF, exportedFontFolderPath, AutoHint=False, RemoveOverlap=True, UseSubroutines=False)
            except Exception as e: # handle any errors while exporting
                print( 'Error while exporting font:\n\t%s' % (str(e)) )
                return
            exportedFontFilePath = exportedFontFolderPath + instance.fileName()
            # If the export was successful...
            if exportResult:
                # Add timestamp to the file name
                exportedFontFilePath = appendTimestampToFilePath( exportedFontFilePath )
                # Add this instance's file path to the list of exported fonts
                exportedFontFiles.append( exportedFontFilePath )
            else:
                print( 'Something went wrong when exporting the font.' ), exportResult
    elif RF :
        # Figure out the exact file path, name, and extension
        fontFilePath = thisFont.path # TODO: Find workaround: this does not seem to work with openning an OTF in RF
        fontFileNameWithExt = os.path.basename( fontFilePath )
        fontFileName, fontFileExtension = os.path.splitext( fontFileNameWithExt )
        # The exported font will be named the UFO file name, without any .ufo extension before the .otf
        exportedFontFilePath = exportedFontFolderPath + fontFileName + '.otf'
        # Export the font, and save the output to a 'report' var
        try:
            exportReport = thisFont.generate(path=exportedFontFilePath, format='otf', decompose=True, checkOutlines=True, releaseMode=False)
        except Exception as e: # handle any errors while exporting
            print( 'Error while exporting font:\n\t%s' % (str(e)) )
        # Check if the export was successful or not
        exportResult = os.path.exists( exportedFontFilePath )
        # If the export was successful...
        if exportResult: #
            # Add timestamp to the file name
            exportedFontFilePath = appendTimestampToFilePath( exportedFontFilePath )
            # Add this instance's file path to the list of exported fonts
            exportedFontFiles.append( exportedFontFilePath )
        else:
            print( 'Something went wrong when exporting the font.' ), exportReport # Note, not exportResult, as for the Glyphs function
            return

def SETUP_installCurrentFont( listOfFontFilePaths ) :
    global fontPSName
    if useCurrentFont == False :
        return

    # Start a list of all installed font files
    global installedFontFiles
    installedFontFiles = []
    for path in listOfFontFilePaths :
        # Try to install the fonts from the file paths list, for Drawbot
        try:
            # Install the font for DrawBot, referenceable by a global variable
            fontPSName = installFont( path )
            # Add this instance's file path to the list of installed fonts
            installedFontFiles.append( path )
        except Exception as e:
            print('Something went wrong when installing the font.')
            print( e )

def UI_setProgressBar( self, val ) :
    # print( 'Progress at %s: %s' % ( str( time.time() ), str(val) ) ) # DEBUG
    self.w.progress.set( val )

def UI_resetProgressBarAfterDelay( self, delay ) :
    # delay as a fraction of a second (ex: .2)
    # import time
    time.sleep( delay )
    UI_setProgressBar( self, 0 )

class NewsprintSimulatorMain( object ) :

    def __init__(self):
        # Some figuring out for the window init
        uiPadding = 20
        uiGutterVert = uiPadding / 2
        uiLabelWidth = 60
        uiButtonHeightReg = 20
        uiButtonsWidth = 170
        uiEditControlWidth = 0 - uiPadding * 2 - uiButtonsWidth - uiPadding
        uiHeight = 127
        winStartWidth = 600
        winStartHeight = uiHeight + winStartWidth * previewAspectRatio + 4
        winMinWidth = 300
        winMinHeight = uiHeight + winMinWidth * previewAspectRatio + 40

        # some vars to reference in callback funcs
        self.content = contentPresets[0]

        # create a window
        self.w = Window(
            (winStartWidth, winStartHeight),
            minSize=(winMinWidth, winMinHeight),
            title = 'Simulate Newsprint',
            #autosaveName = 'newsprintsimulator'
        )
        # Drawbot Canvas Preview Area
        self.w.drawBotCanvas = DrawView((0, uiHeight, -0, -0))
        # Transluscent BG (breaking in RF)
        # self.w.group = Group((0, 0, -0, uiHeight), blendingMode = 'withinWindow')

        yPos = uiPadding + 5
        # Edit the text content
        self.w.contentLabel = TextBox( (uiPadding, yPos + 2, uiLabelWidth, 18), u'Content:', alignment='right' )
        self.w.contentEditor = ComboBox(
            (uiPadding+uiLabelWidth+10, yPos, uiEditControlWidth, 22),
            items = contentPresets,
            continuous = False,
            callback=self.editTextCallback
        )
        self.w.contentEditor.getNSComboBox().setToolTip_('Type something short, then hit the return key to update the preview')
        # Set contentEditor to the first content preset
        self.w.contentEditor.set( contentPresets[0] )
        yPos += 22 + uiPadding - 4

        # add a slider
        self.w.sliderLabel = TextBox( (uiPadding, yPos - 4, uiLabelWidth, 18), u'Intensity:', alignment='right' )
        self.w.slider = Slider(
            (uiPadding+uiLabelWidth+10, yPos, uiEditControlWidth, 23),
            tickMarkCount = 11,
            minValue = 0,
            maxValue = 100,
            value = valAsPercentage( blurDefault, blurMin, blurMax ) ,
            continuous = False,
            callback=self.sliderCallback
        )
        yPos += 23 + uiPadding

        # Progress Bar
        self.w.progress = ProgressBar(
            ( uiPadding, uiHeight - uiGutterVert - 16 - 2, uiEditControlWidth, 16),
            minValue = 0,
            maxValue = 100
        )
        # vertical divider
        self.w.line = VerticalLine(
            ( uiEditControlWidth + uiPadding,
            uiPadding*.5,
            1,
            uiHeight-uiPadding )
        )
        # action buttons
        yPos = uiGutterVert
        self.w.iconFolder = ImageView(
            (0 - uiButtonsWidth - uiPadding, yPos, 16, 16),
            horizontalAlignment = 'center',
            verticalAlignment = 'center',
            scale = 'fit'
        )
        self.w.iconFolder.setImage(imageNamed = 'NSFolder')
        self.w.labelFolder = TextBox(
            (0 - uiButtonsWidth - uiPadding + 16 + 4, yPos + 2, 0-uiPadding, 14),
            u'Folder Name...',
            sizeStyle = 'small'
        )
        self.w.iconFolder.getNSImageView().setToolTip_('The folder for storing font files & PDFs')
        self.w.labelFolder.getNSTextField().setToolTip_('The folder for storing font files & PDFs')
        self.w.buttonChangeFolder = Button(
            (0 - 30 - uiPadding, yPos, 0 - uiPadding, 17),
            u'\u21bb',
            sizeStyle = 'small',
            callback=self.changeFolder
        )
        self.w.buttonChangeFolder.getNSButton().setToolTip_('Change the folder for storing font files & PDFs')
        yPos += uiButtonHeightReg + uiGutterVert - 5 # doesn't need as much gutter, visually
        self.w.buttonReloadFont = Button(
            (0 - uiButtonsWidth - uiPadding, yPos, 0 - uiPadding, uiButtonHeightReg),
            "Reload Font",
            callback=self.reloadCurrentFont
        )
        self.w.buttonReloadFont.getNSButton().setToolTip_('Reload the newsprint preview with the latest version of your font')
        yPos += uiButtonHeightReg + uiGutterVert
        self.w.buttonSavePDF = Button(
            (0 - uiButtonsWidth - uiPadding, yPos, 0 - 5 - 30 - uiPadding, uiButtonHeightReg),
            "Save a PDF",
            callback=self.savePDFCallback
        )
        self.w.buttonSavePDF.getNSButton().setToolTip_('Save a PDF of the newsprint preview, to the folder you chose')
        self.w.buttonRevealPDF = Button(
            (0 - 30 - uiPadding, yPos, 30, uiButtonHeightReg),
            u'\u2192',
            callback=self.revealInFinder
        )
        self.w.buttonRevealPDF.getNSButton().setToolTip_('Reveal your exported PDF file in the Finder')
        self.w.buttonRevealPDF.enable(False)
        yPos += uiButtonHeightReg + uiGutterVert
        self.w.buttonPrint = Button(
            (0 - uiButtonsWidth - uiPadding, yPos, 0 - uiPadding, uiButtonHeightReg),
            "Print It",
            callback=self.printItCallback
        )
        self.w.buttonPrint.getNSButton().setToolTip_('Print the newsprint preview on real paper')
        # Ask for the folder to save to
        SETUP_setProofFolder( self )
        # Set that on the UI's folder label
        self.setFolderLabel( destinationFolderPath )
        # Open the window
        self.w.open()
        # Export the user's font (if in Glyphs or Robofont)
        self.loadCurrentFont()
        # Draw the content
        self.drawIt( savePDF=False )

    def changeFolder( self, sender ) :
        newFolder = SETUP_setProofFolder( self )
        # If the user chose a folder (rather than hitting cancel)
        if newFolder :
            self.setFolderLabel( newFolder )
            # Re-export the user's font (if in Glyphs or Robofont) to that folder
            self.loadCurrentFont()

    def setFolderLabel(self, newFolderPath) :
        folderPath = newFolderPath # set it as the global path variable
        foldersList = folderPath.split('/')
        folderName = foldersList[ len(foldersList) - 1 ] # get name of last folder in path
        self.w.labelFolder.set( folderName ) # update the folder label
        # ensure the set folder button and its label are visible
        #	self.w.textFolderPathLabel.show( True )
        #	self.w.buttonSetFolder.show( True )

    def sliderCallback(self, sender) :
        # slider changed so redraw it
        self.drawIt()

    def editTextCallback(self, sender) :
        newContent = sender.get()
        self.content = newContent
        self.drawIt()

    def savePDFCallback( self, sender ) :
        try :
            exportFilePath = getPDFFilePath()
            self.drawIt(savePDF=True)
        except Exception as e: # handle any errors from PDF export
            print( 'Error while saving the proof as a PDF:\n\t%s' % (str(e)) )
        else :
            global lastExportedProofFilePath
            lastExportedProofFilePath = exportFilePath
            # ensure reveal PDF in finder button is enabled
            self.w.buttonRevealPDF.enable(True)

    def revealInFinder( self, sender ) :
        import subprocess # TODO: should probably move this to the initial dependency check, or wrap in a try/except
        # print "Off to the Finder we go!"
        subprocess.call(["open", "-R", lastExportedProofFilePath])

    def printItCallback( self, sender ) :
        self.drawIt(printIt=True)

    def loadCurrentFont( self ) :
        # just exports it, doesn't install for DB (that happens with drawIt func)
        # Note: before this, run SETUP_setProofFolder( windowThatCalledIt )
        # export the font
        SETUP_exportCurrentFont( destinationFolderPath )

    def reloadCurrentFont( self, sender ) :
        self.loadCurrentFont()
        self.drawIt()

    def drawIt(self, savePDF=False, printIt=False) :
        UI_setProgressBar( self, 0 ) # Update Progress Bar (immediate)
        # pull the font to use, if it was loaded by the font editor
        if useCurrentFont and not DB :
            try :
                SETUP_installCurrentFont( exportedFontFiles )
            except Exception as e:
                print('Something went wrong when installing the font.')
                print( e )
        # get the value from the slider
        intensityRatio = self.w.slider.get()
        blurRange = blurMax - blurMin
        blurAmount = blurMin + blurRange * intensityRatio / 100.0
        contrastRange = contrastMax - contrastMin
        contrastAmount = contrastMin + contrastRange * intensityRatio / 100.0

        # initiate a new drawing
        newDrawing()

        # add a page
        if savePDF or printIt:
            newPage( letterWidth, letterHeight )
            # move/translate the preview content to be centered
            positionPreviewInLetterPage()
        else :
            newPage( previewW, previewH )

        # If not printing or exporting a PDF, add a light gray BG
        if not savePDF and not printIt :
            with savedState():
                fill( 232.0/256 , 232.0/256 , 230.0/256)
                rect(0, 0, previewW, previewH)

        # initiate a new image object
        textImage = ImageObject()
        # draw in the image
        # the 'with' statement will create a custom context
        # only drawing in the image object
        with textImage:
            # set a size for the image
            size(imgW, imgH)
            fill(1, 1, .9)
            rect(0, 0, imgW, imgH)
            # draw something
            fill(0)
            font( fontPSName, fontSizeForImage )
            text( self.content , (10, 100))

        UI_setProgressBar( self, 25 ) # Update Progress Bar (.2s later)

        if not blurAmount == 0 :
            textImage.discBlur( blurAmount )
        if not contrastAmount == 1 :
            textImage.colorControls( contrast = contrastAmount )
        # print('textImage offset:', textImage.offset() )
        # print('textImage size:', textImage.size() )

        # Preview the textImage
        # image( textImage , (0, 0), alpha=.4 )

        # Trace the image of the text
        tracedText = BezierPath()
        tracedText.traceImage( textImage , threshold=0.8, blur=None, invert=False, turd=2, tolerance=0.2, offset=(0, 0))

        UI_setProgressBar( self, 80 ) # Update Progress Bar (image trace can easily take 2s)

        # Add the traced text to the page, at each pt size in the list
        yPos = previewH - 72 * .1
        for index, ptSize in enumerate(ptSizes) :
            # Drop down by the pt size if it's the first line
            if index == 0 :
                yPos -= ptSize
            drawRenderedText( tracedText, xPos=72, yPos=yPos, ptSize=ptSize )
            # visualize the baseline
            # with savedState():
            #     stroke(1, 0, 0, .7)
            #     line( (40, yPos), (650, yPos) )
            # Advance the yPos to the next line + some gutter
            yPos -= ptSize * .3 + 24 #+ ptSize*.5

        # Wrapping Up...
        # If the export button called this, export
        if savePDF :
            exportPDF()
        elif printIt :
            printImage()
        else :
            # get the pdf document
            self.pdfData = pdfImage()
            # set the pdf document into the canvas
            self.w.drawBotCanvas.setPDFDocument( self.pdfData )
        # End the DrawBot drawing
        endDrawing()

        UI_setProgressBar( self, 100 ) # Update Progress Bar (.04s later)
        UI_resetProgressBarAfterDelay( self, .3 ) # Reset Progress Bar

NewsprintSimulatorMain()
